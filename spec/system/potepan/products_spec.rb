require 'rails_helper'

RSpec.describe 'Products', type: :system do
  let(:taxon) { create(:taxon, name: "Bags") }
  let(:product) { create(:product, name: "Python Bag", taxons: [taxon]) }
  let!(:related_product) { create(:product, name: "Ruby on Rails Bag", taxons: [taxon]) }
  let!(:unrelated_product) { create(:product, name: "Mysterious product", taxons: []) }

  scenario "page shows product information and links when taxon attached" do
    visit potepan_product_path product.id
    within ".singleProduct" do
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
      expect(page).to have_content product.description
      expect(page).to have_content "一覧ページへ戻る"
      click_link "一覧ページへ戻る"
    end
    expect(current_path).to eq potepan_category_path(taxon.id)
    click_link product.name
    expect(current_path).to eq potepan_product_path(product.id)
    within ".productsContent" do
      expect(page).to have_content "関連商品"
      expect(page).to have_content related_product.name
      expect(page).to have_content related_product.display_price
      expect(page).not_to have_content unrelated_product.name
      click_link related_product.name
    end
    expect(current_path).to eq potepan_product_path(related_product.id)
  end

  scenario "page has NO taxon infomation when NO taxon attached" do
    visit potepan_product_path unrelated_product.id
    expect(page).to have_content unrelated_product.name
    expect(page).not_to have_content "一覧ページへ戻る"
    expect(page).not_to have_selector ".productBox"
  end
end
