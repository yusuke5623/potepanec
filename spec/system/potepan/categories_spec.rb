require 'rails_helper'

RSpec.describe 'Categories', type: :system do
  let!(:taxonomy) { create(:taxonomy, name: "Categories") }
  let!(:taxon1) { create(:taxon, name: "Bags", taxonomy: taxonomy) }
  let!(:taxon2) { create(:taxon, name: "Mugs", taxonomy: taxonomy) }
  let!(:product1) { create(:product, name: "Python Bag", taxons: [taxon1]) }
  let!(:product2) { create(:product, name: "Ruby on Rails Bag", taxons: [taxon1]) }
  let!(:product3) { create(:product, name: "Ruby on Rails Mug", taxons: [taxon2]) }

  before do
    visit potepan_category_path taxon1.id
  end

  scenario "page shows only products that are relevant to the category" do
    expect(page).to have_content product1.name
    expect(page).to have_content product2.name
    expect(page).not_to have_content product3.name
    expect(page).to have_content product1.display_price
    expect(page).to have_selector("img")
  end

  scenario "page shows only category information that is relevant" do
    expect(page).to have_selector "h2", text: taxon1.name
    expect(page).to have_selector "li", text: taxon1.name
    expect(page).to have_content "商品カテゴリー"
    expect(page).to have_selector "a", text: taxonomy.name
    expect(page).to have_selector "a", text: "#{taxon1.name}　(#{taxon1.products.count})"
    expect(page).to have_selector "a", text: "#{taxon2.name}　(#{taxon2.products.count})"
    click_link taxon2.name
    expect(current_path).to eq potepan_category_path(taxon2.id)
    expect(page).to have_selector "h2", text: taxon2.name
    expect(page).to have_selector "li", text: taxon2.name
  end

  scenario "jump to product page, and then come back to category page" do
    click_link product1.name
    expect(current_path).to eq potepan_product_path(product1.id)
    click_link "一覧ページへ戻る"
    expect(current_path).to eq potepan_category_path(taxon1.id)
  end
end
