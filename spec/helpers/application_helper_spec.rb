require 'rails_helper'

RSpec.describe 'ApplicationHelper' do
  context "when page title is given" do
    it "shows page title" do
      expect(helper.full_title("Python-bag")).to eq("Python-bag - BIGBAG Store")
    end
  end

  context "when NO page title is given" do
    it "only shows base title" do
      expect(helper.full_title("")).to eq("BIGBAG Store")
    end
  end
end
