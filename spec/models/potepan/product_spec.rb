require 'rails_helper'

RSpec.describe 'Product', type: :model do
  let(:taxon) { create(:taxon, name: "Bags") }
  let(:product) { create(:product, name: "Python Bag", taxons: [taxon]) }
  let!(:related_product) { create(:product, name: "Ruby on Rails Bag", taxons: [taxon]) }
  let!(:related_products) { create_list(:product, 4, taxons: [taxon]) }
  let(:unrelated_product) { create(:product, name: "Mysterious product", taxons: []) }
  let(:max_number) { 4 }

  describe "#retrieve_related_products" do
    context "when related product exists" do
      it "shows related product" do
        expect(product.retrieve_related_products(max_number)).to include(related_product)
      end

      it "shows up to 4 related products " do
        expect(product.retrieve_related_products(max_number).count).to eq(4)
      end
    end

    context "when NO related product exists" do
      it "shows related products" do
        expect(unrelated_product.retrieve_related_products(max_number)).not_to include(related_product)
      end
    end
  end
end
