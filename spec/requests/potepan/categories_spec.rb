require 'rails_helper'

RSpec.describe 'Categories', type: :request do
  let!(:taxon) { create(:taxon, name: "Bag") }

  describe "#show" do
    context "when a category exists" do
      before do
        get potepan_category_path taxon.id
      end

      it "returns http success" do
        expect(response).to have_http_status(200)
      end

      it "has standard layout" do
        expect(response.body).to include(potepan_path)
        expect(response.body).to include("header")
        expect(response.body).to include("footer")
      end
    end

    context "when a category does NOT exists" do
      it "returns error" do
        id = Spree::Taxon.last.id + 1
        expect do
          get potepan_category_path(id)
        end.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end
end
