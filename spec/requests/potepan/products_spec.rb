require 'rails_helper'

RSpec.describe "Products", type: :request do
  let(:taxon) { create(:taxon, name: "Bags") }
  let!(:product) { create(:product, name: "Python bag", taxons: [taxon]) }

  describe "#show" do
    context "when a product exists" do
      before do
        get potepan_product_path product.id
      end

      it "returns http success" do
        expect(response).to have_http_status(200)
      end

      it "has standard layout" do
        expect(response.body).to include(potepan_path)
        expect(response.body).to include("header")
        expect(response.body).to include("footer")
      end
    end

    context "when a product does NOT exists" do
      it "returns error" do
        id = Spree::Product.last.id + 1
        expect do
          get potepan_product_path(id)
        end.to raise_error(ActiveRecord::RecordNotFound)
      end
    end
  end
end
