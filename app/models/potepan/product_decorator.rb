module Potepan::ProductDecorator
  def retrieve_related_products(max_number = 4)
    Spree::Product.joins(:taxons).where(
      spree_products_taxons: { taxon_id: taxons }
    ).distinct.where.not(id: id).limit(max_number)
  end

  Spree::Product.prepend self
end
